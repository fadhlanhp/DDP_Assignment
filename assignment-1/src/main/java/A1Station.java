//Import module yang dibutuhkan

import java.util.ArrayList;
import java.util.Scanner;

//Class A1Station
public class A1Station {

    //Instance Variabel
    private static final double THRESHOLD = 250; // in kilograms
    private static ArrayList<TrainCar> ListKereta = new ArrayList<TrainCar>();

    //Method main
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int jumlahBaris = Integer.parseInt(input.nextLine());

        //Untuk berapa banyak cat yang dimasukan
        for (int i = 0; i < jumlahBaris; i++) {
            String temp = input.nextLine();
            String[] splittemp = temp.split(",");
            Double tempWeight = Double.parseDouble(splittemp[1]);
            Double tempLength = Double.parseDouble(splittemp[2]);
            WildCat newCat = new WildCat(splittemp[0], tempWeight, tempLength);

            //ngecek apakah kereta sudah ada isinya atau belum
            if (ListKereta.size() == 0) {
                ListKereta.add(new TrainCar(newCat));
            } else {
                ListKereta.add(new TrainCar(newCat, ListKereta.get(ListKereta.size() - 1)));
            }
            TrainCar lastGerbong = ListKereta.get(ListKereta.size() - 1);

            //ngecek apakah isi kereta sudah penuh
            if (lastGerbong.computeTotalWeight() >= THRESHOLD || i == jumlahBaris - 1) {
                System.out.println("The train departs to Javari Park");
                System.out.print("[LOCO]<--");
                ListKereta.get(ListKereta.size() - 1).printCar();
                double averageMassIndex = lastGerbong.computeTotalMassIndex() / ListKereta.size();
                System.out.println("Average mass index of all cats: " + averageMassIndex);
                System.out.print("In average, the cats in the train are ");

                //ngecek ratarata index massa disuatu kereta
                if (averageMassIndex < 18.5) {
                    System.out.println("underweight");
                } else if (averageMassIndex < 25) {
                    System.out.println("normal");
                } else if (averageMassIndex < 30) {
                    System.out.println("overweight");
                } else {
                    System.out.println("obese");
                }
                ListKereta.clear();
            }
        }
    }
}
