public class WildCat {

    //Instance Variable
    String name;
    double weight; // In kilograms
    double length; // In centimeters

    //Constructor
    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.length = length;
        this.weight = weight;
    }

    //Method ngitung indeks massa satu kucing
    public double computeMassIndex() {
        return this.weight / Math.pow((this.length / 100), 2);
    }
}
