public class TrainCar {

    //Instance Variable
    public static final double EMPTY_WEIGHT = 20; // In kilograms
    private WildCat cat;
    private TrainCar next;

    //Constructor
    public TrainCar(WildCat cat) {
        this.cat = cat;
        this.next = null;

    }

    //Constructor (Overloading)
    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    //Method ngitung total berat dari suatu kereta
    public double computeTotalWeight() {
        if (this.next == null) {
            return (EMPTY_WEIGHT + this.cat.weight);
        } else {
            return (EMPTY_WEIGHT + this.cat.weight) + this.next.computeTotalWeight();
        }
    }

    //Method ngitung ratarata index massa suatu kereta
    public double computeTotalMassIndex() {
        if (this.next == null) {
            return this.cat.computeMassIndex();
        } else {
            return this.cat.computeMassIndex() + this.next.computeTotalMassIndex();
        }
    }

    //Ngeprint apa yang ada di kereta
    public void printCar() {
        if (this.next == null) {
            System.out.println("(" + this.cat.name + ")");
        } else {
            System.out.print("(" + this.cat.name + ")--");
            this.next.printCar();
        }
    }
}
