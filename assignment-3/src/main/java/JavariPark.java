import javari.animal.Animal;
import javari.park.Attraction;
import javari.park.Visitor;
import javari.reader.AnimalRecordsReader;
import javari.reader.AttractionsReader;
import javari.reader.CsvReader;
import javari.reader.SectionReader;
import javari.writer.RegistrationWriter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Ini file mainnya
 * masih sangat berantakan
 *
 * @author Fadhlan Hafizh Permana, 1706040132, DDP Kelas B
 */
public class JavariPark {
    public static void main(String[] args) {
        String directory = "C:\\Users\\Fadhlan Hafizh P\\Desktop\\File Kuliah Semester 2\\DDP Assignment\\assignment-3\\data";
        CsvReader[] files = new CsvReader[3];
        Scanner input = new Scanner(System.in);

        while (true) {
            try {
                File CsvAnimalCategories = new File(directory + "\\animals_categories.csv");
                File CsvAnimalRecords = new File(directory + "\\animals_records.csv");
                File CsvAttractions = new File(directory + "\\animals_attractions.csv");

                files[0] = new SectionReader(CsvAnimalCategories.toPath());
                files[1] = new AnimalRecordsReader(CsvAnimalRecords.toPath());
                files[2] = new AttractionsReader(CsvAttractions.toPath());
                System.out.println("... Loading... Success... System is populating data...\n");
                System.out.println("Found _" + files[0].getValidRecords() + "_ valid sections and _" + files[0].getInvalidRecords() + "_ invalid sections");
                System.out.println("Found _" + files[2].getValidRecords() + "_ valid attractions and _" + files[2].getInvalidRecords() + "_ invalid attractions");
                System.out.println("Found _" + files[0].getValidRecords() + "_ valid animal categories and _" + files[0].getInvalidRecords() + "_ invalid animal categories");
                System.out.println("Found _" + files[1].countValidRecords() + "_ valid animal records and _" + files[1].countInvalidRecords() + "_ invalid animal records\n");
            } catch (IOException e) {
                System.out.println("... Opening default section database from data. ... File not found or incorrect file!\n");
                System.out.print("Please provide the source data path: \n");
                directory = input.nextLine();
            }

            System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
            System.out.println("Please answer the questions by typing the number. Type # if you want to return to the previous menu\n");
            if (checkSection()) {
                System.out.println("Restart!");
                continue;
            }
        }
    }

    public static boolean checkSection() {
        ArrayList<String> temp = SectionReader.getListSection();
        while (true) {
            System.out.println("Javari Park has " + temp.size() + " sections:");
            int count = 1;
            for (String temp2 : temp) {
                System.out.println(count++ + ". " + temp2);
            }
            System.out.print("Please choose your preferred section (type the number): ");
            Scanner input = new Scanner(System.in);
            String inputan = input.nextLine();
            System.out.println();
            if (inputan.equalsIgnoreCase("#")) {
                return true;
            } else {
                int inputtemp = Integer.parseInt(inputan) - 1;
                ArrayList templist = (ArrayList) SectionReader.getListAnimalType().get(SectionReader.getListSection().get(inputtemp));
                int count2 = 1;
                System.out.println("--" + SectionReader.getListSection().get(inputtemp) + "--");
                for (Object temp3 : templist) {
                    System.out.println(count2++ + ". " + temp3);
                }
                while (true) {
                    System.out.print("Please choose your preferred animals (type the number): ");
                    String inputan3 = input.nextLine();
                    if (inputan3.equalsIgnoreCase("#")) {
                        System.out.println();
                        continue;
                    }
                    int intinput = Integer.parseInt(inputan3) - 1;
                    String animal = (String) ((ArrayList) SectionReader.getListAnimalType().get(SectionReader.getListSection().get(inputtemp))).get(intinput);
                    if (checkAnimal(animal)) {
                        break;
                    }
                }
            }
            return true;
        }
    }

    public static boolean checkAnimal(String animal) {
        ArrayList<Attraction> anotherlistAttraction = new ArrayList<>();
        System.out.println();
        System.out.println("--" + animal + "--");
        for (Attraction attraction : AttractionsReader.getListAttractionObject()) {
            for (Animal animal2 : AnimalRecordsReader.getListAnimalObject()) {
                attraction.addPerformer(animal2);
            }
        }
        int countera = 1;
        for (Attraction temp5 : AnimalRecordsReader.getListAttractionObject()) {
            if (!temp5.getPerformers().isEmpty()) {
                if (temp5.getType().equalsIgnoreCase(animal)) {
                    anotherlistAttraction.add(temp5);
                    System.out.println(countera++ + ". " + temp5.getName());
                }
            }
        }
        System.out.print("Please choose your preferred attractions (type the number): ");
        Scanner input = new Scanner(System.in);
        String chooseAttraction = input.nextLine();
        if (chooseAttraction.equalsIgnoreCase("#")) {
            return false;
        }
        Attraction chosen = anotherlistAttraction.get(Integer.parseInt(chooseAttraction) - 1);
        if (finalStep(chosen, animal)) {
            return true;
        }
        return true;
    }

    public static boolean finalStep(Attraction chosen, String animal) {
        ArrayList<Visitor> registeredVisitor = new ArrayList<>();
        System.out.print("\nWow, one more step,\n" + "please let us know your name: ");
        Scanner input = new Scanner(System.in);
        String visitorName = input.nextLine();

        System.out.print("\nYeay, final check!\n" + "Here is your data, and the attraction you chose:\n" + "Name: " + visitorName + "\n" +
                "Attractions: " + chosen.getName() + " -> " + animal + "\n" +
                "With: ");

        int counterc = 0;
        for (Animal performers : chosen.getPerformers()) {
            if (performers.getType().equals(animal)) {
                System.out.print(chosen.getPerformers().get(counterc).getName());
                if (counterc++ != chosen.getPerformers().size() - 1) {
                    System.out.print(", ");
                }
            }
        }
        System.out.println("\n");
        System.out.print("Is the data correct? (Y/N): ");
        String last = input.nextLine();
        if (last.equalsIgnoreCase("N")) {
            System.out.println("Unregistering ticket. Back to main menu.\n");
            return true;
        }
        boolean register = false;
        for (Visitor visitor : registeredVisitor) {
            if (visitor.getVisitorName().equals(visitorName)) {
                visitor.addSelectedAttraction(chosen);
                register = true;
            }
        }
        int countertemp = 1;
        if (!register) {
            Visitor tempvisitor = new Visitor(countertemp++, visitorName);
            registeredVisitor.add(tempvisitor);
            tempvisitor.addSelectedAttraction(chosen);
        }
        System.out.print("\nThank you for your interest. Would you like to register to other attractions? (Y/N): ");
        String anotherTicket = input.nextLine();

        if (anotherTicket.equals("Y")) {
            return true;
        } else {
            String textPath = "C:\\Users\\Fadhlan Hafizh P\\Desktop\\File Kuliah Semester 2\\Latihan Assignment";
            for (Visitor visitortemp : registeredVisitor) {
                try {
                    RegistrationWriter.writeJson(visitortemp, Paths.get(textPath));
                } catch (IOException e) {
                    System.out.println("Something went wrong with " + visitortemp.getVisitorName());
                }
            }
            System.exit(0);
        }
        return false;
    }
}

