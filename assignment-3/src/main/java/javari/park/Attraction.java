package javari.park;

import javari.animal.Animal;

import java.util.ArrayList;

/**
 * Ini class implement dari SelectedAttraction, untuk nyimpen atraksi-atraksi yang ada di Javari Park
 *
 * @author Fadhlan Hafizh Permana, 1706040132, DDP Kelas B
 */

public class Attraction implements SelectedAttraction {
    private String name;
    private String type;
    private ArrayList<Animal> listPerformers = new ArrayList<>();

    public Attraction(String name, String type) {
        this.name = name;
        this.type = type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public ArrayList<Animal> getPerformers() {
        return listPerformers;
    }


    @Override
    public boolean addPerformer(Animal performer) {
        if (performer.getType().equalsIgnoreCase(type) && performer.isShowable()) {
            listPerformers.add(performer);
            return true;
        } else {
            return false;
        }
    }
}

