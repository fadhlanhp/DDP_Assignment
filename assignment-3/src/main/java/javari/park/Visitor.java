package javari.park;

import java.util.ArrayList;

/**
 * Ini class implement dari Registration, untuk bikin object visitor yang ingin masuk ke Javari Park
 *
 * @author Fadhlan Hafizh Permana, 1706040132, DDP Kelas B
 */

public class Visitor implements Registration {
    private int registrationId;
    private String visitorName;
    private ArrayList<SelectedAttraction> selectedAttractions = new ArrayList<>();

    public Visitor(int registrationId, String visitorName) {
        this.registrationId = registrationId;
        this.visitorName = visitorName;
    }

    @Override
    public int getRegistrationId() {
        return registrationId;
    }

    @Override
    public String getVisitorName() {
        return visitorName;
    }

    @Override
    public String setVisitorName(String name) {
        String tempName = this.visitorName;
        this.visitorName = name;
        return tempName + " changed to" + name;
    }

    @Override
    public ArrayList<SelectedAttraction> getSelectedAttractions() {
        return selectedAttractions;
    }

    @Override
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        if (selected == null) {
            return false;
        }
        selectedAttractions.add(selected);
        return true;
    }
}
