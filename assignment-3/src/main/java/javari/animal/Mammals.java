package javari.animal;

/**
 * Ini ChildClass Aves dari Animal
 *
 * @author Fadhlan Hafizh Permana, 1706040132, DDP Kelas B
 */

public class Mammals extends Animal {
    public Mammals(Integer id, String type, String name, Gender gender, double length,
                   double weight, String special, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.specialCondition = special;
    }

    /**
     * @return ngecek apakah dia punya spesifik kondisi untuk tampil
     */
    protected boolean specificCondition() {
        if (this.specialCondition.equalsIgnoreCase("pregnant")) {
            return false;
        }
        if (this.getType().equalsIgnoreCase("Lions") && this.getGender().equals(Gender.FEMALE)) {
            return false;
        }
        return true;
    }
}