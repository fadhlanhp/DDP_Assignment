package javari.animal;

/**
 * Ini ChildClass Aves dari Animal
 *
 * @author Fadhlan Hafizh Permana, 1706040132, DDP Kelas B
 */

public class Aves extends Animal {
    public Aves(Integer id, String type, String name, Gender gender, double length,
                  double weight, String special, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.specialCondition = special;
    }

    protected boolean specificCondition() {
        return specialCondition.equalsIgnoreCase("");
    }
}