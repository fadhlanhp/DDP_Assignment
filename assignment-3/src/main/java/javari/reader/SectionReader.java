package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

/**
 * Ini file buat baca file Section
 * masih sangat berantakan
 *
 * @author Fadhlan Hafizh Permana, 1706040132, DDP Kelas B
 */

public class SectionReader extends CsvReader {


    public SectionReader(Path files) throws IOException {
        super(files);
        this.validRecords = this.countValidRecords();
        this.invalidRecords = this.countInvalidRecords();
        allAnimal();
    }

    private void allAnimal() {
        for (String temp : listSection) {
            ArrayList<String> listbaru = new ArrayList<String>();
            for (String sections : getLines()) {
                String[] temp2 = sections.split(COMMA);
                if (!listbaru.contains(temp2[0])) {
                    if (temp.equalsIgnoreCase(temp2[2])) {
                        listbaru.add(temp2[0]);
                    }
                }
            }
            listAnimalType.put(temp, listbaru);
        }
    }


    /**
     * Counts the number of valid records from read CSV file.
     *
     * @return
     */
    public long countValidRecords() {
        for (String sections : getLines()) {
            String[] temp = sections.split(COMMA);
            if (!listSection.contains(temp[2])) {
                listSection.add(temp[2]);
            }
            if (!listCategories.contains(temp[1])) {
                listCategories.add(temp[1]);
            }
            if (!listAnimal.contains(temp[0])) {
                listAnimal.add(temp[0]);
            }
        }
        return listSection.size();
    }

    /**
     * Counts the number of invalid records from read CSV file.
     *
     * @return
     */
    public long countInvalidRecords() {
        long invalid = 0;
        for (String temp : getLines()) {
            String[] listtemp = temp.split(COMMA);
            if (listtemp.length != 3) {
                invalid += 1;
            }
        }
        return invalid;
    }

    public long getValidRecords() {
        return validRecords;
    }

    public long getInvalidRecords() {
        return invalidRecords;
    }
}


