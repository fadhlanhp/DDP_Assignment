package javari.reader;

import javari.park.Attraction;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Ini file buat baca file Attraction
 * masih sangat berantakan
 *
 * @author Fadhlan Hafizh Permana, 1706040132, DDP Kelas B
 */
public class AttractionsReader extends CsvReader {

    public AttractionsReader(Path files) throws IOException{
        super(files);
        this.validRecords = countValidRecords();
        this.invalidRecords = countInvalidRecords();
        putAnimalAttractions(listAnimal);
    }


    public void putAnimalAttractions (ArrayList<String> listAllAnimal){
        for (String temp : listAllAnimal){
            ArrayList<String> listtemp = new ArrayList<>();
            for (String temp2 : getLines()){
                String[] temp3 = temp2.split(COMMA);
                if (temp.equalsIgnoreCase(temp3[0])){
                    listtemp.add(temp3[1]);
                }
            }

            listAnimalAttractions.put(temp, listtemp);
        }
        for (String temp4 : getLines()){
            String[] temp5 = temp4.split(COMMA);
            listAttractionObject.add(new Attraction(temp5[1],temp5[0]));
        }
    }

    public long getValidRecords() {
        return validRecords;
    }

    public long getInvalidRecords() {
        return invalidRecords;
    }

    public long countValidRecords() {
        for (String temp : getLines()) {
            String[] listtemp = temp.split(COMMA);
            if (!listAttractions.contains(listtemp[1])) {
                listAttractions.add(listtemp[1]);
            }
        }
        return listAttractions.size();
    }
    /**
     * Counts the number of invalid records from read CSV file.
     *
     * @return
     */
    public long countInvalidRecords() {
        long invalid = 0;
        for (String temp : getLines()) {
            String[] listtemp = temp.split(COMMA);
            if (listtemp.length != 2) {
                invalid += 1;
            }
        }
        return invalid;
    }
}
