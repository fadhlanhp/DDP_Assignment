package javari.reader;

import javari.animal.*;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Ini file buat baca file Animal Records
 * masih sangat berantakan
 *
 * @author Fadhlan Hafizh Permana, 1706040132, DDP Kelas B
 */

public class AnimalRecordsReader extends CsvReader {

    public AnimalRecordsReader(Path file) throws IOException {
        super(file);
        addAnimalObject();
    }

    public String nameAnimal(String type) {
        String allanimaltype = "";
        for (Animal temp : listAnimalObject) {
            if (temp.getType().equalsIgnoreCase(type)) {
                allanimaltype += temp.getName() + ", ";
            }
        }
        return allanimaltype;
    }


    private void addAnimalObject() {
        for (String temp : getLines()) {
            String[] data = temp.split(COMMA);
            if (listAnimal.contains(data[1])) {
                if (data[1].equals("Cat") || data[1].equals("Lion") || data[1].equals("Hamster") || data[1].equals("Whale")) {
                    listAnimalObject.add(new Mammals(Integer.parseInt(data[0]), data[1], data[2], Gender.parseGender(data[3]), Double.parseDouble(data[4]), Double.parseDouble(data[5]), data[6], Condition.parseCondition(data[7])));
                }
                if (data[1].equals("Parrot") || data[1].equals("Eagle")) {
                    listAnimalObject.add(new Aves(Integer.parseInt(data[0]), data[1], data[2], Gender.parseGender(data[3]), Double.parseDouble(data[4]), Double.parseDouble(data[5]), data[6], Condition.parseCondition(data[7])));
                }
                if (data[1].equals("Snake")) {
                    listAnimalObject.add(new Reptiles(Integer.parseInt(data[0]), data[1], data[2], Gender.parseGender(data[3]), Double.parseDouble(data[4]), Double.parseDouble(data[5]), data[6], Condition.parseCondition(data[7])));
                }
            }
        }
    }

    public long countValidRecords() {
        long valid = 0;
        for (String record : lines) {
            String[] data = record.split(COMMA);
            if (data.length == 8) {
                valid += 1;
            }
        }
        return valid;
    }

    public long countInvalidRecords() {
        long invalid = 0;
        for (String record : lines) {
            String[] data = record.split(COMMA);
            if (data.length != 8) {
                invalid += 1;
            }
        }
        return invalid;
    }

}
