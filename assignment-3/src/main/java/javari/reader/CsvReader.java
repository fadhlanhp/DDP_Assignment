package javari.reader;

import javari.animal.Animal;
import javari.park.Attraction;

import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class represents the base class for reading all lines from a text
 * file that contains CSV data.
 *
 * @author Programming Foundations 2 Teaching Team
 * @author TODO If you make changes in this class, please write your name here
 * and describe the changes in the comment block
 */
public abstract class CsvReader {

    protected static ArrayList<String> listSection = new ArrayList<>();
    protected static ArrayList<String> listCategories = new ArrayList<>();
    protected static Map listAnimalType = new HashMap<String, ArrayList<String>>();
    protected static ArrayList<String> listAnimal = new ArrayList<>();
    protected static Map listAnimalAttractions = new HashMap<String, ArrayList<String>>();
    protected static ArrayList<String> listAttractions = new ArrayList<>();
    protected static ArrayList<Animal> listAnimalObject = new ArrayList<>();
    protected static ArrayList<Attraction> listAttractionObject = new ArrayList<>();

    protected long validRecords;
    protected long invalidRecords;

    public static final String COMMA = ",";

    private final Path file;
    protected final List<String> lines;

    public static ArrayList<Attraction> getListAttractionObject() {
        return listAttractionObject;
    }

    public static ArrayList<String> getListSection() {
        return listSection;
    }

    public static ArrayList<String> getListCategories() {
        return listCategories;
    }

    public static Map getListAnimalType() {
        return listAnimalType;
    }

    public static ArrayList<String> getListAnimal() {
        return listAnimal;
    }

    public static Map getListAnimalAttractions() {
        return listAnimalAttractions;
    }

    public static ArrayList<String> getListAttractions() {
        return listAttractions;
    }

    public static ArrayList<Animal> getListAnimalObject() {
        return listAnimalObject;
    }

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *                     properly
     */
    public CsvReader(Path file) throws IOException {
        this.file = file;
        this.lines = Files.readAllLines(this.file, StandardCharsets.UTF_8);
    }

    /**
     * Returns all line of text from CSV file as a list.
     *
     * @return
     */
    public List<String> getLines() {
        return lines;
    }

    /**
     * Counts the number of valid records from read CSV file.
     *
     * @return
     */
    public abstract long countValidRecords();

    /**
     * Counts the number of invalid records from read CSV file.
     *
     * @return
     */
    public abstract long countInvalidRecords();

    public long getValidRecords() {
        return validRecords;
    }

    public long getInvalidRecords() {
        return invalidRecords;
    }

}
