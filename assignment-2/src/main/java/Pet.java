class Pet extends Animal {
    Pet(String name, int length) {
        super(name, length);
        this.place = "indoor";

        //untuk menentukan cage code dari setiap hewan
        if (this.getLength() < 45) {
            this.cageCode = 'A';
        } else if (this.getLength() <= 60) {
            this.cageCode = 'B';
        } else {
            this.cageCode = 'C';
        }
    }
}