class Eagle extends Wild {

   Eagle(String name, int length) {
        super(name, length);
    }

    //Method action
    public void action(int number) {
        if (number == 1){
            this.ordertoFly();
        }
        else {
            System.out.println("You do nothing...\n" + "Back to the office!\n");
        }
    }

    private void ordertoFly() {
        System.out.println(this.getName() + "makes a voice: kwaakk...\n" +
                "You hurt!\n" + "Back to the office!\n");
    }
}