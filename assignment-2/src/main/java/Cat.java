import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Cat extends Pet {

    //Constructor of Cat
    Cat(String name, int length) {
        super(name, length);
    }

    //Method action
    public void action(int number) {
        if (number == 1){
            this.brushTheFur();
        }
        else if (number == 2){
            this.Cuddle();
        }
        else {
            System.out.println("You do nothing...\n" + "Back to the office!\n");
        }
    }

    private void brushTheFur() {

        System.out.println("Time to clean " + this.getName() + "'s fur\n" +
                this.getName() + "makes a voice: Nyaaan...\n" +
                "Back to the office!\n");
    }

    private void Cuddle() {
        List<String> listSound = Arrays.asList("Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!");
        Random rand = new Random();
        int number = rand.nextInt(3);
        System.out.println(this.getName() + " makes a voice: " + listSound.get(number) + "\n" +
                "Back to the office!\n");
    }

}