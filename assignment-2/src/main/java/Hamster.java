class Hamster extends Pet {

    Hamster(String name, int length) {
        super(name, length);
    }

    //Method action
    public void action(int number) {
        if (number == 1) {
            this.seeItGnawing();
        } else if (number == 2) {
            this.ordertoRun();
        } else {
            System.out.println("You do nothing...");
        }
        System.out.println("Back to the office!\n");
    }

    private void seeItGnawing() {
        System.out.println(this.getName() + " makes a voice: ngkkrit.. ngkkrrriiit");
    }

    private void ordertoRun() {
        System.out.println(this.getName() + " makes a voice: trrr…. trrr...");
    }
}
