public class Animal {

    //Instance Variable of Animal
    private String name;
    private int length;
    char cageCode;
    String place;

    //Constructor of Animal
    Animal(String name, int length) {
        this.name = name;
        this.length = length;
    }

    //Getter dari variabel variabel
    public String getName() {
        return name;
    }

    public char getCageCode() {
        return cageCode;
    }

    public int getLength() {
        return length;
    }

    public String getPlace() {
        return place;
    }

    //Sebuah method kosong agar bisa di overriding oleh masing masing child class
    public void action(int number) {
    }

}