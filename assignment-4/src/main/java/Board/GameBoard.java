package Board;

import Card.GameCard;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

public class GameBoard {
    /**
     * An ArrayList contains all of gamecard (36 gameCard)
     */
    private ArrayList<GameCard> allGameCard = new ArrayList<>();

    /**
     * Frame in GUI
     */
    private JFrame boardFrame;

    /**
     * Panel for all of game card
     */
    private JPanel boardPanel = new JPanel();

    /**
     * temp variable Card
     */
    private GameCard firstCard, secondCard;

    /**
     * Constructor of Game Board
     */
    public GameBoard() {
        ArrayList<Byte> cardID = new ArrayList<>();
        for (byte i = 1; i <= 18; i++) {
            cardID.add(i);
            cardID.add(i);
        }
        Collections.shuffle(cardID);

        for (byte tempID : cardID) {
            try {
                ImageIcon firstIcon = new ImageIcon("C:\\Users\\Fadhlan Hafizh P\\Desktop\\File Kuliah Semester 2\\DDP Assignment\\assignment-4\\img\\Logo.jpg");
                ImageIcon secondIcon = new ImageIcon("C:\\Users\\Fadhlan Hafizh P\\Desktop\\File Kuliah Semester 2\\DDP Assignment\\assignment-4\\img\\Character\\" +
                        tempID + ".jpg");
                ImageIcon thirdIcon = new ImageIcon("C:\\Users\\Fadhlan Hafizh P\\Desktop\\File Kuliah Semester 2\\DDP Assignment\\assignment-4\\img\\Together.jpg");
                ImageIcon resizeFirstIcon = new ImageIcon(firstIcon.getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH));
                ImageIcon resizeSecondIcon = new ImageIcon(secondIcon.getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH));
                ImageIcon resizeThirdIcon = new ImageIcon(thirdIcon.getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH));

                GameCard card = new GameCard(tempID, resizeFirstIcon, resizeSecondIcon, resizeThirdIcon);
                card.getGameButton().addActionListener(actions -> clickedCard(card));
                allGameCard.add(card);
            }
            catch (Exception e){
                System.out.println("Something Wrong with The Game!");
            }
        }

        boardFrame = new JFrame("Kingdom Hearts! Mathcing Cards Game");
        boardFrame.setLayout(new BorderLayout());
        boardPanel.setLayout(new GridLayout(6, 6));
        creatMenu();

        for (GameCard tempCard : allGameCard) {
            boardPanel.add(tempCard.getGameButton());
        }

        boardFrame.add(boardPanel, BorderLayout.PAGE_START);
        boardFrame.pack();
        boardFrame.setSize(600, 720);
        boardFrame.setVisible(true);
        boardFrame.setResizable(false);


        Timer firstTimer = new Timer(1500, actions -> revealCard());
        firstTimer.start();
        firstTimer.setRepeats(false);
    }

    /**
     * To Open All of Card in board that hasnt been reveal
     */
    private void revealCard() {
        for (GameCard tempCard : allGameCard) {
            tempCard.reveal();
        }
        Timer secondTimer = new Timer(1500, actions -> hideCard());
        secondTimer.start();
        secondTimer.setRepeats(false);
    }

    /**
     * to close all of card
     */
    private void hideCard() {
        for (GameCard tempCard : allGameCard) {
            tempCard.hide();
        }
    }

    /**
     * create menu in GUI
     */
    private void creatMenu() {
        JMenu boardMenu;
        JMenuItem restartMenu, exitMenu;
        JMenuBar menuBar = new JMenuBar();
        boardMenu = new JMenu("Menu");
        restartMenu = new JMenuItem("Restart");
        exitMenu = new JMenuItem("Exit");
        exitMenu.addActionListener(actions -> exit());
        restartMenu.addActionListener(actions -> restart());
        boardMenu.add(restartMenu);
        boardMenu.add(exitMenu);
        menuBar.add(boardMenu);
        boardFrame.setJMenuBar(menuBar);
    }

    /**
     * exit the game
     */
    private void exit() {
        System.exit(0);
    }

    /**
     * restart the game
     * masih error. bukan error sih, tp ngehang pas ngerestartnya
     */
    private void restart() {
        boardFrame.remove(boardPanel);
        Collections.shuffle(allGameCard);
        boardPanel = new JPanel(new GridLayout(6, 6));
        for (GameCard tempCard : allGameCard) {
            tempCard.restart();
            boardPanel.add(tempCard.getGameButton());
        }
        boardFrame.add(boardPanel, BorderLayout.PAGE_START);
        revealCard();
    }

    /**
     * method that used if the card is clicked
     * @param tempCard card that has been clicked
     */
    private void clickedCard(GameCard tempCard) {
        if (firstCard == null) {
            firstCard = tempCard;
            firstCard.reveal();
        }

        if (firstCard != null && firstCard != tempCard && secondCard == null) {
            secondCard = tempCard;
            secondCard.reveal();
            Timer thirdTimer = new Timer(500, actions -> check());
            thirdTimer.start();
            thirdTimer.setRepeats(false);
        }
    }

    /**
     * to check if two cards have same ID
     */
    private void check() {
        if (secondCard.getId() == firstCard.getId()) {
            firstCard.getGameButton().setEnabled(false);
            secondCard.getGameButton().setEnabled(false);
            firstCard.setMatched(true);
            secondCard.setMatched(true);
            firstCard.correct();
            secondCard.correct();
            if (isGameOver()) {
                exit();
            }
        } else {
            firstCard.hide();
            secondCard.hide();
        }
        firstCard = null;
        secondCard = null;
    }

    /**
     * to check if the game is over or not
     * @return gameover or not
     */
    private boolean isGameOver() {
        for (GameCard tempCard : allGameCard) {
            if (!tempCard.isMatched()) {
                return false;
            }
        }
        return true;
    }

}
