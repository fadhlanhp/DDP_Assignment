package Card;

import javax.swing.*;

public class GameCard {
    /**
     * if of the card
     */
    private byte id;

    /**
     * image if the button isnt open
     */
    private ImageIcon first;

    /**
     * image if the button is open
     */
    private ImageIcon second;

    /**
     * image if the button is matched
     */
    private ImageIcon third;

    /**
     * boolean to see if the card is checked of not
     */
    private boolean isMatched;

    /**
     * GUI button for the cards
     */
    private JButton gameButton = new JButton();

    /**
     * Contructor of the card
     * @param id ID of the card
     * @param first first image of the card
     * @param second real image of the card
     * @param third last image of the card
     */
    public GameCard(byte id, ImageIcon first, ImageIcon second, ImageIcon third) {
        this.id = id;
        this.first = first;
        this.second = second;
        this.third = third;
        this.isMatched = false;
        this.gameButton.setIcon(first);
    }

    /**
     * to hide real image of card
     */
    public void hide() {
        this.gameButton.setIcon(first);
    }

    /**
     * to show real image of card
     */
    public void reveal() {
        this.gameButton.setIcon(second);
    }

    /**
     * show last image if the card is matching with other
     */
    public void correct() {
        this.gameButton.setIcon(third);
    }

    /**
     * getter ID of the card
     * @return ID of Card
     */
    public byte getId() {
        return id;
    }

    /**
     * restart button
     */
    public void restart() {
        getGameButton().setEnabled(true);
        setMatched(false);
        hide();
    }

    /**
     * Getter matched of the card
     * @return to see if the card alreasy matched with other card
     */
    public boolean isMatched() {
        return isMatched;
    }

    /**
     * set the card to false or true
     * @param matched true or false
     */
    public void setMatched(boolean matched) {
        isMatched = matched;
    }

    /**
     * get GUI button of this card
     * @return
     */
    public JButton getGameButton() {
        return gameButton;
    }
}
